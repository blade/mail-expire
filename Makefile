
DATE_FMT = +%Y-%m-%d
ifdef SOURCE_DATE_EPOCH
    DATE ?= $(shell date -u -d "@$(SOURCE_DATE_EPOCH)" "$(DATE_FMT)" 2>/dev/null || date -u -r "$(SOURCE_DATE_EPOCH)" "$(DATE_FMT)" 2>/dev/null || date -u "$(DATE_FMT)")
else
    DATE ?= $(shell date "$(DATE_FMT)")
endif
PACKAGE=mail-expire
VERSION=$(shell head -n1 debian/changelog | cut -f2 -d'(' | cut -f1 -d')')

all: $(PACKAGE).1

$(PACKAGE).1: $(PACKAGE) debian/changelog
	pod2man --center="User Commands" --date="$(DATE)" --errors="none" \
		--fixed="CW" \
		--fixedbold="CB" \
		--fixeditalic="CI" \
		--fixedbolditalic="CX" \
		--nourls \
		--release="$(PACKAGE) $(VERSION)" \
		--section="1" \
		$< $@

